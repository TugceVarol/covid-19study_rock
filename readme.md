# University students’ beliefs about a COVID-19 vaccine scenario within the university and communications about COVID-19 vaccine 

The overarching aim of the project that this study is a part of is to develop an intervention promoting COVID-19 vaccination among university students through informed decision making. Moreover, by promoting COVID-19 vaccination and exploring the beliefs of the university students with regard to a vaccination policy that could be enforced in a university setting, we aimed to create a safe environment for students. The development of an intervention aiming at behavior change requires several steps to accomplish. 

In our study, first, we aimed to select the relevant determinants (and beliefs) of university students' intention to get the COVID-19 vaccine to specify the change objectives of our intervention. Please note that by COVID-19 vaccine, we mean the COVID-19 vaccines approved for use in Europe. This part of the study has been reported separately and is not within the scope of this pre-registration. Due to the broad focus, we decided to parse the studies into two while analyzing the data and communicating the findings. The first paper was limited to the quantitative parts of the survey. This pre-registration and the following paper concern the qualitative parts of the survey, which require thorough coding and analysis.

Second, in order to facilitate the informed decision making around the COVID-19 vaccination, we aim to gather information on a) students’ information needs around COVID-19 vaccination, b) the type of communication channels through which they would like to receive the information, and c) what students consider trusted sources of information. This part is aimed at generating knowledge. (Part A)

Imposing the COVID-19 vaccines as a prerequisite to enter certain facilities is a debate ongoing worldwide. Our third and final aim is to explore if the COVID-19 vaccine requirement was implemented within a university setting, what would be the reactions of the students to this policy. We asked students if they agree or disagree (on a 7-point Likert scale) with the statement “Only people who are already vaccinated should be allowed to come on-site (UM buildings) in case that current COVID-19 prevention guidelines still apply.” and requested to elaborate their response in writing. This part is aimed at both generating knowledge and developing policy. (Part B)

Please note that this pre-registration concerns parts A and B of the study, the qualitative parts. 


The rendered Rmd file is hosted by GitLab Pages at https://tugcevarol.gitlab.io/covid-19study_rock

<<<<<<< HEAD
Link to the OSF repo: https://osf.io/2q435/
=======
The Open Science Framework repository for this project is located at [Link to the OSF repo]
>>>>>>> 9ca6b7ffb87794fb67e7839892cce86fb496a098

The preregistration form is integrated in the Rmd file, but the frozen preregistation file is at [Link to the prereg]

